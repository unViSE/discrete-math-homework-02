package com.penzstu.unvise.task1;

import com.penzstu.unvise.core.algorithm.Dijkstra;
import com.penzstu.unvise.core.Edge;
import com.penzstu.unvise.core.Vertex;

public class Main {
    public static void main(String[] args) {
        Vertex v1 = new Vertex("x1");
        Vertex v2 = new Vertex("x2");
        Vertex v3 = new Vertex("x3");
        Vertex v4 = new Vertex("x4");
        Vertex v5 = new Vertex("x5");
        Vertex v6 = new Vertex("x6");

        v1.addNeighbour(new Edge(5, v1, v2));
        v1.addNeighbour(new Edge(8, v1, v3));
        v1.addNeighbour(new Edge(7, v1, v4));
        v1.addNeighbour(new Edge(18, v1, v5));

        v2.addNeighbour(new Edge(11, v2, v3));

        v3.addNeighbour(new Edge(17, v3, v6));

        v4.addNeighbour(new Edge(10, v4, v2));
        v4.addNeighbour(new Edge(12, v4, v3));
        v4.addNeighbour(new Edge(6, v4, v5));

        v5.addNeighbour(new Edge(7, v5, v2));
        v5.addNeighbour(new Edge(8, v5, v3));
        v5.addNeighbour(new Edge(11, v5, v6));

        Dijkstra.computePath(v1);

        System.out.println(Dijkstra.getShortestPathTo(v6));
        System.out.println(Dijkstra.getMinimalWeightTo(v6));
    }

}
