package com.penzstu.unvise.task2;

import com.penzstu.unvise.core.Edge;
import com.penzstu.unvise.core.algorithm.Kruskal;
import com.penzstu.unvise.core.Vertex;
import com.penzstu.unvise.utils.Vertices;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        Vertex v1 = new Vertex("X1", 1);
        Vertex v2 = new Vertex("X2", 2);
        Vertex v3 = new Vertex("X3", 3);
        Vertex v4 = new Vertex("X4", 4);
        Vertex v5 = new Vertex("X5", 5);
        Vertex v6 = new Vertex("X6", 6);
        Vertex v7 = new Vertex("X7", 7);

        v1.addNeighbour(new Edge(10, v1, v2));
        v1.addNeighbour(new Edge(11, v1, v3));
        v1.addNeighbour(new Edge(14, v1, v5));
        v1.addNeighbour(new Edge(12, v1, v7));

        v2.addNeighbour(new Edge(10, v2, v1));
        v2.addNeighbour(new Edge(10, v2, v3));
        v2.addNeighbour(new Edge(9, v2, v4));
        v2.addNeighbour(new Edge(7, v2, v7));

        v3.addNeighbour(new Edge(11, v3, v1));
        v3.addNeighbour(new Edge(10, v3, v2));
        v3.addNeighbour(new Edge(12, v3, v4));
        v3.addNeighbour(new Edge(10, v3, v5));
        v3.addNeighbour(new Edge(6, v3, v7));

        v4.addNeighbour(new Edge(9, v4, v2));
        v4.addNeighbour(new Edge(12, v4, v3));
        v4.addNeighbour(new Edge(9, v4, v5));
        v4.addNeighbour(new Edge(12, v4, v6));

        v5.addNeighbour(new Edge(14, v5, v1));
        v5.addNeighbour(new Edge(10, v5, v3));
        v5.addNeighbour(new Edge(9, v5, v4));
        v5.addNeighbour(new Edge(11, v5, v6));
        v5.addNeighbour(new Edge(12, v5, v7));

        v6.addNeighbour(new Edge(12, v6, v4));
        v6.addNeighbour(new Edge(11, v6, v5));

        v7.addNeighbour(new Edge(12, v7, v1));
        v7.addNeighbour(new Edge(7, v7, v2));
        v7.addNeighbour(new Edge(6, v7, v3));
        v7.addNeighbour(new Edge(12, v7, v5));

        List<Edge> list = Vertices.getAllEdges(
                v1, v2, v3, v4, v5, v6, v7
        );

        Kruskal.compute(list);
    }

}
