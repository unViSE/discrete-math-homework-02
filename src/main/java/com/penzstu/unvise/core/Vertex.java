package com.penzstu.unvise.core;

import java.util.ArrayList;
import java.util.List;

public class Vertex implements Comparable<Vertex> {
    
    private final String name;
    private int nameAsInteger;
    private List<Edge> edges;
    private boolean visited;
    private Vertex previousVertex;
    private double minDistance = Double.MAX_VALUE;

    public Vertex(String name) {
        this.name = name;
        this.edges = new ArrayList<>();
    }
    public Vertex(String name, int nameAsInteger) {
        this.name = name;
        this.nameAsInteger = nameAsInteger;
        this.edges = new ArrayList<>();
    }

    public void addNeighbour(Edge edge) {
        this.edges.add(edge);
    }

    public String getName() {
        return name;
    }

    public int getNameAsInteger() {
        return nameAsInteger;
    }

    public void setNameAsInteger(int nameAsInteger) {
        this.nameAsInteger = nameAsInteger;
    }

    public List<Edge> getEdges() {
        return edges;
    }

    public void setEdges(List<Edge> edges) {
        this.edges = edges;
    }

    public boolean isVisited() {
        return visited;
    }

    public void setVisited(boolean visited) {
        this.visited = visited;
    }

    public Vertex getPreviousVertex() {
        return previousVertex;
    }

    public void setPreviousVertex(Vertex previousVertex) {
        this.previousVertex = previousVertex;
    }

    public double getMinDistance() {
        return minDistance;
    }

    public void setMinDistance(double minDistance) {
        this.minDistance = minDistance;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int compareTo(Vertex o) {
        return Double.compare(minDistance, o.minDistance);
    }

}