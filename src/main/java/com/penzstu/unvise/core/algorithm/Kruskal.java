package com.penzstu.unvise.core.algorithm;

import com.penzstu.unvise.core.DisjointSet;
import com.penzstu.unvise.core.Edge;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Kruskal {
    public static void compute(List<Edge> edges) {
        int verticesCount = edges.size();
        StringBuilder outputMessage = new StringBuilder();

        edges.sort(Comparator.comparingDouble(Edge::getWeight));
        ArrayList<Edge> mstEdges = new ArrayList<>();

        // Initialize singleton sets for each node in graph. (nodeCount +1) to account for arrays indexing from 0
        DisjointSet verticesSet = new DisjointSet(verticesCount + 1);

        // Loop over all edges. Start @ 1 (ignore 0th as placeholder). Also, early termination when number of edges=(number of nodes-1)
        for (int i = 0; i < edges.size() && mstEdges.size() < (verticesCount - 1); i++) {
            Edge currentEdge = edges.get(i);

            int root = verticesSet.find(currentEdge.getStartVertex().getNameAsInteger());
            int otherRoot = verticesSet.find(currentEdge.getTargetVertex().getNameAsInteger());

            // If roots are in different sets the DO NOT create a cycle
            if (root != otherRoot) {
                mstEdges.add(currentEdge);
                verticesSet.union(root, otherRoot);
            }
        }

        outputMessage.append("\nFinal Minimum Spanning Tree (").append(mstEdges.size()).append(" edges)\n");
        int mstTotalEdgeWeight = 0;
        for (Edge edge : mstEdges) {
            outputMessage.append(edge).append("\n");
            mstTotalEdgeWeight += edge.getWeight();
        }
        outputMessage.append("\nTotal weight of all edges in MST = ").append(mstTotalEdgeWeight);

        System.out.println(outputMessage);

    }
}