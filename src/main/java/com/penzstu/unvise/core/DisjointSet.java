package com.penzstu.unvise.core;

import java.util.Arrays;

public class DisjointSet {

    private final int[] set;

    public DisjointSet(int numElements) {
        set = new int[numElements];
        Arrays.fill(set, -1);
    }

    public void union(int root, int otherRoot) {
        if (set[otherRoot] < set[root])
            set[root] = otherRoot;
        else {
            if (set[root] == set[otherRoot])
                set[root]--;
            set[otherRoot] = root;
        }
    }

    public int find(int x) {
        if (set[x] < 0)
            return x;
        else
            return set[x] = find(set[x]);
    }

}
