package com.penzstu.unvise.utils;

import com.penzstu.unvise.core.Edge;
import com.penzstu.unvise.core.Vertex;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Vertices {

    public static List<Edge> getAllEdges(Vertex... vertices) {
        List<Edge> result = new ArrayList<>();
        Arrays.stream(vertices).forEach(x -> result.addAll(x.getEdges()));
        return result;
    }

}
